package com.saveme.siemens.siemenssaveme;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

public class GForceMeter extends Activity implements SensorEventListener, OnClickListener,LocationListener {
    private static CalcThread calcThread;

    private static TextView gCurTxt, gMinTxt, gMaxTxt,gForceTxt,maxGForceTxt, speedTxt,maxSpeedTxt,oldSpeedTxt;

    private static SensorManager sensorMgr = null;

    private static boolean calibrate = false;

    private Button calibrateButton, exitButton;

    private static String TAG;

    private float lastUpdate;
    private LocationManager locManager;
    private double maxSpeed=0.0;
    private double gForce =0.0;
    private double oldSpeed =0.0;
    boolean isAccident=false;
    private String phoneNumber="9560739697";
    private Double speedThreshhold=0.0;
    private Double gForceTHreshhold=0.0;
    private String LAT="";
    private String LONG="";

    private class MyHandler extends Handler
    {
        public void handleMessage(Message msg)
        {
            if(isAccident)
                return;

            String[] values = (String[])msg.obj;
            gCurTxt.setText(values[0]);
            gMinTxt.setText(values[1]);
            gMaxTxt.setText(values[2]);
            gForceTxt.setText(values[3]);
            maxGForceTxt.setText(values[4]);
            maxSpeedTxt.setText(values[5]);
            speedTxt.setText(values[6]);
            oldSpeedTxt.setText(values[7]);
            LAT = values[8];
            LONG = values[9];

            gForce =Double.valueOf(values[3]);
            maxSpeed = Double.valueOf(values[5]);
            Double currentSpeed =  Double.valueOf(values[6]);

            if((currentSpeed>= speedThreshhold || oldSpeed>= speedThreshhold) &&  gForce >= gForceTHreshhold) {

                isAccident = true;
                stopSensing();
                sendNotifictaion();

            }

        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gforce_meter);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        phoneNumber = intent.getStringExtra("phoneNumber");
        speedThreshhold=Double.valueOf(intent.getStringExtra("speed"));
        gForceTHreshhold=Double.valueOf(intent.getStringExtra("gforce"));


        TAG = getString(R.string.app_name);

        calibrateButton = (Button)findViewById(R.id.CalibrateButton);
        calibrateButton.setOnClickListener(this);

        exitButton = (Button)findViewById(R.id.ExitButton);
        exitButton.setOnClickListener(this);

        gCurTxt = (TextView)findViewById(R.id.gCurTxt);
        gMinTxt = (TextView)findViewById(R.id.gMinTxt);
        gMaxTxt = (TextView)findViewById(R.id.gMaxTxt);
        gForceTxt = (TextView)findViewById(R.id.gForceTxt);
        maxGForceTxt = (TextView)findViewById(R.id.maxGForceTxt);
        speedTxt = (TextView)findViewById(R.id.speedTxt);
        maxSpeedTxt=(TextView)findViewById(R.id.maxSpeedTxt);
        oldSpeedTxt=(TextView)findViewById(R.id.oldSpeedTxt);
        lastUpdate = 0;
        calibrate = true;
        startSensing();
        Log.d(TAG, "onCreate done");
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {
        // TODO Auto-generated method stub
    }

    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub

        if(isAccident)
            return;
        switch(event.sensor.getType())
        {
            case Sensor.TYPE_ACCELEROMETER:
            {


                float currTime = System.currentTimeMillis();
                if(currTime < (lastUpdate+500))
                    return;

                lastUpdate = currTime;

                if(calcThread == null)
                {
                    Log.d(TAG, "CalcThread not running");
                    return;
                }

                Handler h = calcThread.getHandler();
                if(h == null)
                {
                    Log.e(TAG, "Failed to get CalcThread Handler");
                    return;
                }

                Message m = Message.obtain(h);
                if(m == null)
                {
                    Log.e(TAG, "Failed to get Message instance");
                    return;
                }

                m.obj = (Object)event.values;
                if(calibrate)
                {
                    calibrate = false;

                    m.what = CalcThread.CALIBRATE;
                    h.sendMessageAtFrontOfQueue(m);
                }
                else
                {
                    m.what = CalcThread.GRAVITY_CHANGE;
                    m.obj = (Object)event.values;
                    m.sendToTarget();
                }

                break;
            }
        }
    }

    private void startSensing() {
        if (calcThread == null) {
            calcThread = new CalcThread(TAG, new MyHandler());
            calcThread.start();
        }

        if (sensorMgr == null) {
            sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
            Sensor sensor = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

            if (!sensorMgr.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI)) {
                // on accelerometer on this device
                Log.e(TAG, "No accelerometer available");
                sensorMgr.unregisterListener(this, sensor);
                sensorMgr = null;
                return;
            }
        }

        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }
        }

        if (locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            }

        }



        calibrate = true;
    }

    @Override
    public void onLocationChanged(Location location) {
        if(isAccident)
            return;
        Handler h = calcThread.getHandler();
        Message m = Message.obtain(h);
        Double[] values = new Double[]
                {Double.valueOf(location.getSpeed()),location.getLatitude(),location.getLongitude()};

        m.obj = values;
        m.what = CalcThread.SPEED_CHANGE;
        m.sendToTarget();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    private void stopSensing()
    {
        if(sensorMgr != null)
        {
            sensorMgr.unregisterListener(this);
            sensorMgr = null;
        }

        if(locManager != null)
        {
            locManager.removeUpdates(this);
            locManager = null;
        }

        if(calcThread != null)
        {
            Handler h = calcThread.getHandler();
            if(h != null)
            {
                Message m = Message.obtain(h);
                if(m != null)
                {
                    m.what = CalcThread.SENSOR_STOP;
                    h.sendMessageAtFrontOfQueue(m);
                }
            }

            calcThread = null;
        }
    }

    public void onClick(View arg0) {
        // TODO Auto-generated method stub

        if(arg0.getId() == R.id.CalibrateButton)
        {
            Log.d(TAG, "----Calibrate button clicked----");
            calibrate = true;
        }
        else if(arg0.getId() == R.id.ExitButton)
        {
            Log.d(TAG, "----Exit button clicked----");
            stopSensing();
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "----onPause called----");
        //stopSensing();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "----onResume called----");
        isAccident =false;
        //startSensing();
    }


    private void sendNotifictaion() {

        //Send SMS
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNumber,null, getAddress() ,null,null);

        //Phone Call
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+phoneNumber));
        callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        callIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Toast.makeText(getApplicationContext(), "Make a phone call", Toast.LENGTH_SHORT).show();
        GForceMeter.this.startActivity(callIntent);

    }


    private String getAddress(){

        String address = "Emergency!!! I have met with an accident with gForce: "+ gForce + " average speed: "+maxSpeed  +
                 "send help at" +"\n" +"588 Block A"+ "\n" +"Sector 28 Gurugram" +"\n Haryana 122022";
        //String address ="588 Block A Sushant Lok Phase I, Sector 28 Gurugram, Haryana 122022";
        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        try{

            List<Address> addressList = geoCoder.getFromLocation(Double.valueOf(LAT),Double.valueOf(LONG),1);
            if(addressList !=null){
                Address returnAddress = addressList.get(0);
                StringBuilder builderAddtress = new StringBuilder();
                for (int i=0;i< returnAddress.getMaxAddressLineIndex();i++){
                    builderAddtress.append(returnAddress.getAddressLine(i)).append("\n");
                }
                address = builderAddtress.toString();
            }


        }
        catch(Exception e){

        }
        return  address;
    }


}