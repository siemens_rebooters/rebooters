package com.saveme.siemens.siemenssaveme;

import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class CalcThread extends Thread
{
    public static final int CALIBRATE			= 0;
    public static final int SENSOR_STOP			= 1;
    public static final int GRAVITY_CHANGE		= 2;
    public static final int SPEED_CHANGE = 3;

    private class MyHandler extends Handler
    {
        public void handleMessage(Message msg)
        {

            if(gOffsetX == null && msg.what != SPEED_CHANGE)
            {
                float[] values = (float[]) msg.obj;
                gOffsetX = (Float)values[0];
                gOffsetY = (Float)values[1];
                gOffsetZ = (Float)values[2] - SensorManager.GRAVITY_EARTH;
            }
            switch(msg.what)
            {

                case GRAVITY_CHANGE:
                {
                    mHandler.removeMessages(GRAVITY_CHANGE);

                    float[] values = (float[]) msg.obj;
                    float curValY = (values[1]- gOffsetY) / SensorManager.GRAVITY_EARTH;
                    if(curValY >= (lastValY - 0.01) || curValY <= (lastValY + 0.01))
                    {
                        lastValY = curValY;
                        Log.d(TAG, "Updating sensor data");

                       // updateTextViews();
                    }
                    else
                        Log.v(TAG, "Difference not enough");

                    float curValX = (values[0]- gOffsetX) / SensorManager.GRAVITY_EARTH;
                    if(curValX >= (lastValX - 0.01) || curValX <= (lastValX + 0.01))
                    {
                        lastValX = curValX;
                        Log.d(TAG, "Updating sensor data");

                       // updateTextViews();
                    }
                    else
                        Log.v(TAG, "Difference not enough");

                    float curValZ = (values[2]- gOffsetZ) / SensorManager.GRAVITY_EARTH;
                    if(curValZ >= (lastValZ - 0.01) || curValZ <= (lastValZ+ 0.01))
                    {
                        lastValZ = curValZ;
                        Log.d(TAG, "Updating sensor data");


                    }
                    else
                        Log.v(TAG, "Difference not enough");
                    float netAcc = (float) Math.sqrt((double) (lastValX * lastValX + lastValY * lastValY + lastValZ * lastValZ));
                    accelerationGForce = netAcc/SensorManager.GRAVITY_EARTH;

                    if(accelerationGForce > maxAccGForce){
                        maxAccGForce= accelerationGForce;
                    }
                    updateTextViews();


                    break;
                }

                case SENSOR_STOP:
                {
                    Log.d(TAG, "Sensor stopping");
                    Looper.myLooper().quit();
                    mHandler = null;
                    break;
                }
                case SPEED_CHANGE:
                {
                    //queue.add(speed);
                    Double[] values = (Double[]) msg.obj;

                    latitude = values [1];
                    longitude = values [2];

                    oldSpeed = speed;
                    speed = values [0];
                    if(speed > maxSpeed)
                        maxSpeed = speed;


                    updateTextViews();
                    break;
                }
                default:
                {
                    Log.e(TAG, "Got unknown message type " + Integer.toString(msg.what));
                    break;
                }
            }
        }
    }

    private float lastValX = 1.0f;
    private Float gOffsetX = null;

    private float lastValY = 1.0f;
    private Float gOffsetY = null;

    private float lastValZ = 1.0f;
    private Float gOffsetZ = null;

    private float maxAccGForce=0.0f;
    private float accelerationGForce=0.0f;

    private double speed = 0.0f;
    private double maxSpeed = 0.0f;
    private double oldSpeed = 0.0f;

    private double latitude = 0.0f;
    private double longitude = 0.0f;

    private Handler uiHandler;
    private static Handler mHandler;

    private String TAG;

    CalcThread(String TAG, Handler uiHandler)
    {
        super("CalcThread");
        this.uiHandler = uiHandler;
        this.TAG = TAG;
    }

    public Handler getHandler()
    {
        return mHandler;
    }

    private void updateTextViews()
    {
        Message m = uiHandler.obtainMessage();
        String values[] = {
                String.format("%+.2f", lastValX),
                String.format("%+.2f", lastValY),
                String.format("%+.2f", lastValZ),
                String.format("%+.2f", accelerationGForce),
                String.format("%+.2f", maxAccGForce),
                String.format("%+.2f", maxSpeed),
                String.format("%+.2f", speed),
                String.format("%+.2f", oldSpeed),
                String.format("%+.2f", latitude),
                String.format("%+.2f", longitude)

        };

        m.obj = (Object)values;
        m.sendToTarget();
    }

    public void run()
    {
        // TODO Auto-generated method stub
        Looper.prepare();
        Log.v(TAG, "Starting CalcThread");
        mHandler = new MyHandler();
        Looper.loop();
        Log.v(TAG, "Past Looper.loop()");
    }
}
