package com.saveme.siemens.siemenssaveme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    private Intent myIntentGMeter;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.v("Debug", "Activity started..");

    }

    public void saveMe(View view) {
        Log.v("Debug", "Save Clicked..");

        EditText phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        EditText speed = (EditText) findViewById(R.id.txtSpeed);
        EditText gforce = (EditText) findViewById(R.id.txtGForce);

        if(phoneNumber.getText().toString().length() == 10 && !"".equals(speed.getText().toString().length()) && !"".equals(gforce.getText().toString().length())) {
            myIntentGMeter = new Intent(this, GForceMeter.class);
            myIntentGMeter.putExtra("phoneNumber", phoneNumber.getText().toString());
            myIntentGMeter.putExtra("speed", speed.getText().toString());
            myIntentGMeter.putExtra("gforce", gforce.getText().toString());
            startActivity(myIntentGMeter);
        }


    }




}